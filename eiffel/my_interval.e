note
	description: "Esame di Sviluppo software in gruppi di lavoro complessi"
	author: "Mattia Monga"

deferred class
	MY_INTERVAL

feature

	make (l, u: INTEGER)
			-- Set bounds to l and u;
			-- make interval empty if l > u.
		deferred
		ensure
			lower_set: lower = l
			upper_set: upper = u
			empty_interval: (l > u) = is_empty
		end

feature -- Access

	lower: INTEGER

	upper: INTEGER

feature -- Comparison

	is_comparable (other: like Current): BOOLEAN
			-- Is either one of current interval and other
			-- strictly contained in the other?
		deferred
		ensure
			definition: Result = (Current < other) or ((Current ~ other)) or (Current > other)
		end

	is_subinterval alias "<" (other: like Current): BOOLEAN
			-- Is current interval strictly included in other?
		deferred
		ensure
			definition: lower > other.lower and upper < other.upper
		end

	is_superinterval alias ">" (other: like Current): BOOLEAN
			-- Does current interval strictly include other?
		deferred
		ensure
			definition: other < Current
		end

	extend_to (i: INTEGER)
			-- estende l'intervallo fino a includere il parametro i
		require
			(i < lower or upper < i)
		do
			if i < lower then
				lower := i
			else
				upper := i
			end
		ensure
			new_lower_bound: i < old lower implies i = lower
			new_upper_bound: i > old upper implies i = upper
			wider_or_equal: (Current ~ old Current) or else (Current > old Current)
		end

feature -- Status report

	is_empty: BOOLEAN
			-- Does interval contain no values?
		do
			Result := lower > upper
		end

invariant
	empty_if_no_values: is_empty = (lower > upper)

end
